/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/index.js":
/*!*********************!*\
  !*** ./js/index.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// grab a reference of our \"canvas\" using its id\r\nconst canvas = document.getElementById('canvas');\r\n/* get a \"context\". Without \"context\", we can't draw on canvas */\r\nconst ctx = canvas.getContext('2d');\r\n\r\n// some sounds\r\nconst hitSound = new Audio('../sounds/hitSound.wav');\r\nconst scoreSound = new Audio('../sounds/scoreSound.wav');\r\nconst wallHitSound = new Audio('../sounds/wallHitSound.wav');\r\n\r\n/* some extra variables */\r\nconst netWidth = 4;\r\nconst netHeight = canvas.height;\r\n\r\nconst paddleWidth = 10;\r\nconst paddleHeight = 100;\r\n\r\nlet upArrowPressed = false;\r\nlet downArrowPressed = false;\r\n\r\n/* some extra variables ends */\r\n\r\n/* objects */\r\n// net\r\nconst net = {\r\n  x: canvas.width / 2 - netWidth / 2,\r\n  y: 0,\r\n  width: netWidth,\r\n  height: netHeight,\r\n  color: \"#FFF\"\r\n};\r\n\r\n// user paddle\r\nconst user = {\r\n  x: 10,\r\n  y: canvas.height / 2 - paddleHeight / 2,\r\n  width: paddleWidth,\r\n  height: paddleHeight,\r\n  color: '#FFF',\r\n  score: 0\r\n};\r\n\r\nconst ai = {\r\n  x: canvas.width - (paddleWidth + 10),\r\n  y: canvas.height / 2 - paddleHeight / 2,\r\n  width: paddleWidth,\r\n  height: paddleHeight,\r\n  color: '#FFF',\r\n  score: 0\r\n};\r\n\r\n// ball\r\nconst ball = {\r\n  x: canvas.width / 2,\r\n  y: canvas.height / 2,\r\n  radius: 7,\r\n  speed: 7,\r\n  velocityX: 5,\r\n  velocityY: 5,\r\n  color: '#05EDFF'\r\n};\r\n\r\n/* objects declaration ends */\r\n\r\n/* drawing functions */\r\n// function to draw net\r\nfunction drawNet() {\r\n  // set the color of net\r\n  ctx.fillStyle = net.color;\r\n\r\n  // syntax --> fillRect(x, y, width, height)\r\n  ctx.fillRect(net.x, net.y, net.width, net.height);\r\n}\r\n\r\n// function to draw score\r\nfunction drawScore(x, y, score) {\r\n  ctx.fillStyle = '#fff';\r\n  ctx.font = '35px sans-serif';\r\n\r\n  // syntax --> fillText(text, x, y)\r\n  ctx.fillText(score, x, y);\r\n}\r\n\r\n// function to draw paddle\r\nfunction drawPaddle(x, y, width, height, color) {\r\n  ctx.fillStyle = color;\r\n  ctx.fillRect(x, y, width, height);\r\n}\r\n\r\n// function to draw ball\r\nfunction drawBall(x, y, radius, color) {\r\n  ctx.fillStyle = color;\r\n  ctx.beginPath();\r\n  // syntax --> arc(x, y, radius, startAngle, endAngle, antiClockwise_or_not)\r\n  ctx.arc(x, y, radius, 0, Math.PI * 2, true); // π * 2 Radians = 360 degrees\r\n  ctx.closePath();\r\n  ctx.fill();\r\n}\r\n\r\n/* drawing functions end */\r\n\r\n/* moving Paddles */\r\n// add an eventListener to browser window\r\nwindow.addEventListener('keydown', keyDownHandler);\r\nwindow.addEventListener('keyup', keyUpHandler);\r\n\r\n// gets activated when we press down a key\r\nfunction keyDownHandler(event) {\r\n  // get the keyCode\r\n  switch (event.keyCode) {\r\n    // \"up arrow\" key\r\n    case 38:\r\n      // set upArrowPressed = true\r\n      upArrowPressed = true;\r\n      break;\r\n    // \"down arrow\" key\r\n    case 40:\r\n      downArrowPressed = true;\r\n      break;\r\n  }\r\n}\r\n\r\n// gets activated when we release the key\r\nfunction keyUpHandler(event) {\r\n  switch (event.keyCode) {\r\n    // \"up arraow\" key\r\n    case 38:\r\n      upArrowPressed = false;\r\n      break;\r\n    // \"down arrow\" key\r\n    case 40:\r\n      downArrowPressed = false;\r\n      break;\r\n  }\r\n}\r\n\r\n/* moving paddles section end */\r\n\r\n// reset the ball\r\nfunction reset() {\r\n  // reset ball's value to older values\r\n  ball.x = canvas.width / 2;\r\n  ball.y = canvas.height / 2;\r\n  \r\n\r\n\r\n  console.log(ball.speed);\r\n  // changes the direction of ball\r\n  ball.velocityX = -ball.velocityX;\r\n  ball.velocityY = -ball.velocityY;\r\n}\r\n\r\n// collision Detect function\r\nfunction collisionDetect(player, ball) {\r\n  // returns true or false\r\n  player.top = player.y;\r\n  player.right = player.x + player.width;\r\n  player.bottom = player.y + player.height;\r\n  player.left = player.x;\r\n\r\n  ball.top = ball.y - ball.radius;\r\n  ball.right = ball.x + ball.radius;\r\n  ball.bottom = ball.y + ball.radius;\r\n  ball.left = ball.x - ball.radius;\r\n\r\n  return ball.left < player.right && ball.top < player.bottom && ball.right > player.left && ball.bottom > player.top;\r\n}\r\n\r\n// update function, to update things position\r\nfunction update() {\r\n  // move the paddle\r\n  if (upArrowPressed && user.y > 0) {\r\n    user.y -= 8;\r\n  } else if (downArrowPressed && (user.y < canvas.height - user.height)) {\r\n    user.y += 8;\r\n  }\r\n\r\n  // check if ball hits top or bottom wall\r\n  if (ball.y + ball.radius >= canvas.height || ball.y - ball.radius <= 0) {\r\n    // play wallHitSound\r\n    //wallHitSound.play();\r\n    ball.velocityY = -ball.velocityY;\r\n  }\r\n\r\n   // if ball hit on right wall\r\n   if (ball.x + ball.radius >= canvas.width) {\r\n    // play scoreSound\r\n    //scoreSound.play();\r\n    // then user scored 1 point\r\n    user.score += 1;\r\n    reset();\r\n  }\r\n\r\n  // if ball hit on left wall\r\n  if (ball.x - ball.radius <= 0) {\r\n    // play scoreSound\r\n    //scoreSound.play();\r\n    // then ai scored 1 point\r\n    ai.score += 1;\r\n    reset();\r\n  }\r\n\r\n  // move the ball\r\n  ball.x += ball.velocityX;\r\n  ball.y += ball.velocityY;\r\n\r\n  // ai paddle movement\r\n  ai.y += ((ball.y - (ai.y + ai.height / 2))) * 0.09;\r\n\r\n  // collision detection on paddles\r\n  let player = (ball.x < canvas.width / 2) ? user : ai;\r\n\r\n  if (collisionDetect(player, ball)) {\r\n    // play hitSound\r\n    //hitSound.play();\r\n    // default angle is 0deg in Radian\r\n    let angle = 0;\r\n\r\n    // if ball hit the top of paddle\r\n    if (ball.y < (player.y + player.height / 2)) {\r\n      // then -1 * Math.PI / 4 = -45deg\r\n      angle = -1 * Math.PI / 4;\r\n    } else if (ball.y > (player.y + player.height / 2)) {\r\n      // if it hit the bottom of paddle\r\n      // then angle will be Math.PI / 4 = 45deg\r\n      angle = Math.PI / 4;\r\n    }\r\n\r\n    /* change velocity of ball according to on which paddle the ball hitted */\r\n    ball.velocityX = (player === user ? 1 : -1) * ball.speed * Math.cos(angle);\r\n    ball.velocityY = ball.speed * Math.sin(angle);\r\n\r\n    // increase ball speed\r\n    if (ai.score > 10 ) {\r\n        ball.speed ++;\r\n        console.log(ball.speed);\r\n    }\r\n    if (user.score > 10 ) {\r\n        ball.speed ++;\r\n        console.log(ball.speed);\r\n    }\r\n  }\r\n}\r\n\r\n// render function draws everything on to canvas\r\nfunction render() {\r\n  // set a style\r\n  ctx.fillStyle = \"#000\"; /* whatever comes below this acquires black color (#000). */\r\n  // draws the black board\r\n  ctx.fillRect(0, 0, canvas.width, canvas.height);\r\n\r\n  // draw net\r\n  drawNet();\r\n\r\n  // draw user score\r\n  drawScore(canvas.width / 4, canvas.height / 6, user.score);\r\n  // draw ai score\r\n  drawScore(3 * canvas.width / 4, canvas.height / 6, ai.score);\r\n  // draw user paddle\r\n  drawPaddle(user.x, user.y, user.width, user.height, user.color);\r\n  // draw ai paddle\r\n  drawPaddle(ai.x, ai.y, ai.width, ai.height, ai.color);\r\n  // draw ball\r\n  drawBall(ball.x, ball.y, ball.radius, ball.color);\r\n}\r\n\r\nfunction gameOver() {\r\n    if(ai.score === 2 ) {\r\n        ctx.clearRect(\r\n\t\t\t0,\r\n\t\t\t0,\r\n\t\t\tthis.canvas.width,\r\n\t\t\tthis.canvas.height\r\n\t\t);\r\n        \r\n    }\r\n}\r\n\r\ndocument.body.onkeydown = function(e){\r\n    if(e.keyCode == 32){\r\n        document.querySelector('.placeholder p').classList.add (\"hide\");\r\n    // gameLoop\r\n    function gameLoop() {\r\n        gameOver();\r\n        // update() function here\r\n        update();\r\n        // render() function here\r\n        render();\r\n        \r\n    }\r\n    gameLoop();\r\n    // calls gameLoop() function 60 times per second\r\n    setInterval(gameLoop, 1000 / 60);\r\n   }\r\n}\n\n//# sourceURL=webpack:///./js/index.js?");

/***/ })

/******/ });